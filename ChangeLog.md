# Geo-Location for TYPO3 Change-Log

## 2023-09-08 Release of Version 1.6.6

*   [TASK] Fix small spelling/language errors
*   [TASK] Add documentations configuration



## 2021-01-04 Release of Version 1.6.5

*   [TASK] Add german translation



## 2020-06-04 Release of Version 1.6.4

*   [TASK] Add extra tags in composer.json



## 2020-05-25 Release of Version 1.6.3

*   [TASK] Cleanup Change-Log



## 2019-10-13  Release of version 1.6.2

*	[TASK] Remove DEV identifier.
*	[TASK] Add Gitlab-CI configuration.



## 2017-09-28  Release of version 1.6.1



## 2017-04-03  Release of version 1.2.1

*	[TASK] License key



## 2017-04-02  Release of version 1.2.0

*	[FEATURE] Add formatted address



## 2017-03-21  Release of version 1.1.1

*	[BUGFIX] Fixing countAll on GeoLocationRepository



## 2017-03-09  Release of version 1.1.0

*	[FEATURE] Geo-Location records are now stored in different pages/container, so that they can be grouped.
*	[FEATURE] Adding a findAllByGeoCoordinatesInDistance Repository method, for getting Geo-Locations



## 2017-03-05  Release of version 1.0.0
