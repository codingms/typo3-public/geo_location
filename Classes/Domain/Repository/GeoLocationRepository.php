<?php
namespace CodingMs\GeoLocation\Domain\Repository;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * The repository for Geo Locations
 */
class GeoLocationRepository extends Repository
{

    /**
     * Find Geo-Location by Address
     *
     * @param string $address Address
     * @return mixed
     */
    public function findByAddress($address = '')
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $constraint = $query->equals('address', $address);
        return $query->matching($constraint)->execute()->getFirst();
    }

    /**
     * Find Geo-Location by Address and Page uid
     *
     * @param string $address Address
     * @param int $pageUid Page where the records should be stored. If empty, the rootpage will be used!
     * @return mixed
     */
    public function findByAddressAndPageUid($address = '', $pageUid=0)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $constraints = array();
        $constraints[] = $query->equals('address', $address);
        $constraints[] = $query->equals('pid', $pageUid);
        $constraint = $query->logicalAnd($constraints);
        return $query->matching($constraint)->execute()->getFirst();
    }

    /**
     * @param $settings array
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllBySettings($settings=array()) {
        $query = $this->createQuery();
        $latitude = 0.0;
        if(isset($settings['latitude'])) {
            $latitude = (float)$settings['latitude'];
        }
        $longitude = 0.0;
        if(isset($settings['longitude'])) {
            $longitude = (float)$settings['longitude'];
        }
        // Search distance
        $distance = isset($settings['distance']) ? (int)$settings['distance'] : 50;
        $offset = isset($settings['offset']) ? (int)$settings['offset'] : 0;
        $limit = isset($settings['limit']) ? (int)$settings['limit'] : 9999;
        // To search by kilometers instead of miles, replace 3959 with 6371.
        $distanceUnit = 6371;
        if((bool)$settings['inMiles']) {
            $distanceUnit = 3959;
        }
        // Page uid array
        $pageWhere = '';
        if(isset($settings['pageUids']) && is_array($settings['pageUids'])) {
            $validatedPageUids = array();
            if(count($settings['pageUids'])>0) {
                foreach($settings['pageUids'] as $pageUid) {
                    $validatedPageUids[] = (int)$pageUid;
                }
                $pageWhere = ' AND pid IN (' . implode(',', $validatedPageUids) . ') ';
            }
        }
        // Build statement
        $sql = 'SELECT ';
        $sql .= '*, (' . $distanceUnit . ' * acos( cos( radians(' . $latitude . ') ) * cos( radians(`latitude`) ) * ';
        $sql .= 'cos( radians(`longitude`) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * ';
        $sql .= 'sin( radians(`latitude`) ) ) ) AS distance FROM tx_geolocation_domain_model_geolocation ';
        $sql .= 'HAVING distance < ' . $distance . $pageWhere .  ' ORDER BY distance LIMIT ' . $offset . ', ' . $limit;
        $query->statement($sql);
        $result = $query->execute();
        return $result;
    }

}