<?php
namespace CodingMs\GeoLocation\Service;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\Exception;
use CodingMs\GeoLocation\Domain\Model\GeoLocation;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * Services for Geo-Locations
 *
 * @package geo_location
 * @subpackage Service
 *
 * @author Thomas Deuling <typo3@coding.ms>
 * @since 1.0.0
 */
class GeoLocationService
{

    /**
     * @var \CodingMs\GeoLocation\Domain\Repository\GeoLocationRepository
     * @inject
     */
    protected $geoLocationRepository = null;

    /**
     * @var string Webservice host, with subsequent slash!
     */
    protected $webserviceHost = 'http://maps.googleapis.com/maps/api/';

    /**
     * Initialize repository
     * @throws Exception
     */
    public function initializeObject()
    {
        // Check curl requirement
        if (!function_exists('curl_version')) {
            throw new Exception('CURL required!');
        }
    }

    /**
     * @param string $webservice
     * @param string $action
     * @param array $params
     * @return mixed
     * @throws Exception
     */
    protected function getData($webservice = '', $action = '', array $params = array())
    {
        // Build query string from params
        $queryString = $this->buildQueryString($params);
        // Build webservice url
        $url = $this->webserviceHost . $webservice . '/' . $action . $queryString;
        // Init CURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5000);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $data = curl_exec($ch);
        if (curl_exec($ch) === false) {
            throw new Exception('Curl error: ' . curl_error($ch) . ' (' . $url . ')');
        }
        curl_close($ch);
        return $data;
    }

    /**
     * Get user agent
     * @return array|string
     */
    protected function detectBrowser()
    {
        if (empty($_SERVER['HTTP_USER_AGENT'])) {
            return array(
                'name' => 'unrecognized',
                'version' => 'unknown',
                'platform' => 'unrecognized',
                'userAgent' => ''
            );
        }
        return $_SERVER['HTTP_USER_AGENT'];
    }

    /**
     * Build query string for webservice request
     * @param array $params
     * @return string
     */
    protected function buildQueryString(array $params = array())
    {
        $queryString = '';
        if (!empty($params)) {
            $queryStringParts = array();
            foreach ($params as $key => $value) {
                $queryStringParts[] = $key . '=' . urlencode($value);
            }
            $queryString = '?' . implode('&', $queryStringParts);
        }
        return $queryString;
    }

    /**
     * Find geo coordinates by address or postal code
     * @param string $address
     * @param int $pageUid Page where the records should be stored. If empty, the rootpage will be used!
     * @return \CodingMs\GeoLocation\Domain\Model\GeoLocation
     * @throws Exception
     */
    public function findByAddress($address = '', $pageUid=0)
    {
        /** @var \CodingMs\GeoLocation\Domain\Model\GeoLocation $geoLocation */
        $geoLocation = $this->geoLocationRepository->findByAddressAndPageUid($address, $pageUid);
        if (!($geoLocation instanceof GeoLocation)) {
            // Prepare params
            $params = array();
            $params['address'] = $address;
            $params['sensor'] = 'true';
            // Request data
            $result = $this->getData('geocode', 'json', $params);
            // Decode json
            if ($result = json_decode($result, true)) {
                // https://developers.google.com/maps/documentation/geocoding/intro?hl=de
                if($result['status'] == 'OK') {
                    if(isset($result['results']) && count($result['results']) > 0) {
                        foreach($result['results'] as $result) {
                            /** @var ObjectManager $objectManager */
                            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
                            /** @var \CodingMs\GeoLocation\Domain\Model\GeoLocation $geoLocation */
                            $geoLocation = $objectManager->get(GeoLocation::class);
                            $geoLocation->setPid($pageUid);
                            $geoLocation->setAddress($address);
                            $geoLocation->setFormattedAddress($result['formatted_address']);
                            $geoLocation->setLatitude($result['geometry']['location']['lat']);
                            $geoLocation->setLongitude($result['geometry']['location']['lng']);
                            $this->geoLocationRepository->add($geoLocation);
                            /** @var PersistenceManager $persistenceManager */
                            $persistenceManager = $objectManager->get(PersistenceManager::class);
                            $persistenceManager->persistAll();
                        }
                    }
                }
            }
        }
        return $geoLocation;
    }

}
