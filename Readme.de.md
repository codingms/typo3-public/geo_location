# Geo-Location für TYPO3

Dieses Tool konvertiert Adressen in Geo-Locations mit Hilfe der Google-Maps API. Diese Geo-Locations werden dann in der Datenbank gespeichert, sodass diese nicht immer und immer wieder abgerufen werden müssen. Dies ist nützlich für Erweiterungen die einen solchen Adress zu geo-Location Service benötigen.

## Verwendung

Verwenden Sie einfach den Service um eine Geo-Location für eine Adresse zu ermitteln. Wenn diese Adresse bereits in der lokalen Datenbank vorhanden ist, wird die zugehörige Geo-Location verwendet - wenn nicht werden die Geo-Koordinaten über die Google-Maps API abgerufen und dann in der lokalen Datenbank abgelegt.

```php
/** @var \CodingMs\GeoLocation\Domain\Model\GeoLocation $geoLocation */
$geoLocation = $this->geoLocationService->findByAddress('48161 Münster');
if($geoLocation instanceof \CodingMs\GeoLocation\Domain\Model\GeoLocation) {
	$yourObject->setGeoLocation($geoLocation);
}

```