# Geo-Location for TYPO3

Converts Addresses into Geo-Locations by Google-Maps API. Saving this Geo-Locations in an own Database for prevent converting this again and again. Useful for extensions which needs such a Address to Geo-Coordinates service.

## Usage

Just use the service for getting the Geo-Location for an address. When it's already in local Database, it will be used - otherwise the Google-Maps API is called for receiving the Geo-Coordinates, which will be saved afterwards in database.

```php
/** @var \CodingMs\GeoLocation\Domain\Model\GeoLocation $geoLocation */
$geoLocation = $this->geoLocationService->findByAddress('48161 Münster');
if($geoLocation instanceof \CodingMs\GeoLocation\Domain\Model\GeoLocation) {
	$yourObject->setGeoLocation($geoLocation);
}

```