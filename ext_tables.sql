#
# Table structure for table 'tx_geolocation_domain_model_geolocation'
#
CREATE TABLE tx_geolocation_domain_model_geolocation (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	address varchar(255) DEFAULT '' NOT NULL,
	formatted_address varchar(255) DEFAULT '' NOT NULL,
	latitude decimal(10,8) DEFAULT '0.00000000' NOT NULL,
	longitude decimal(11,8) DEFAULT '0.00000000' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),

);
