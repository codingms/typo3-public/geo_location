<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_geolocation_domain_model_geolocation', 'EXT:geo_location/Resources/Private/Language/locallang_csh_tx_geolocation_domain_model_geolocation.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_geolocation_domain_model_geolocation');
